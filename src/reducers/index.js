import { combineReducers } from 'redux';

import math from './mathReducer';
import auth from './authReducer';

export default combineReducers({
    math,
    auth
});