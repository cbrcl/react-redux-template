import * as types from "../constants/actionTypes";


const initialState = {
    value: 1
}

const mathReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.ADD_ONE:
            state = {
                ...state,
                value: state.value + action.payload
            }

            break;
        case types.SUBTRACT_ONE:
            state = {
                ...state,
                value: state.value - action.payload
            }
            break;
        case types.DOUBLE_VALUE:
            state = {
                ...state,
                value: state.value * action.payload
            }
            break;
        default:
            break;

    }

    return state;
};

export default mathReducer;