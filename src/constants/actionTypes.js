export const ADD_ONE = "ADD_ONE";
export const SUBTRACT_ONE = "SUBTRACT_ONE";
export const DOUBLE_VALUE = "DOUBLE_VALUE";

export const SET_CURRENT_USER = "SET_CURRENT_USER";