const url="http://localhost:5000/api/";

function handleResponse(response) {
    if (response.ok) {
        return response.json();
    } else {
        let error = new Error(response.statusText);
        error.response = response;
        throw error;
    }
}

export default class Api {
    static post(endpoint, data={}){
        return fetch(url+endpoint, {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer "+localStorage.getItem('jwtToken')
            }
        }).then(handleResponse);
    }

    static get(endpoint){
        return fetch(url+endpoint, {
            method: 'get',
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer "+localStorage.getItem('jwtToken')
            }
        }).then(handleResponse);
    }
}
