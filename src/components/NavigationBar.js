import React from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logout } from '../actions/authActions';
import { Link } from 'react-router-dom';


class NavigationBar extends React.Component {
    render() {
        const {isAuthenticated} = this.props.auth;

        const userLinks = (
            <Nav className="ml-auto" navbar>
                <NavItem>
                    <NavLink href="/logout" to="/logout" tag={Link}>Logout</NavLink>
                </NavItem>
            </Nav>
        );

        const guestLinks = (
            <Nav className="ml-auto" navbar>
                <NavItem>
                    <NavLink href="/login" to="/login" tag={Link}>Login</NavLink>
                </NavItem>
            </Nav>
        );

        return (
            <div>
                <Navbar color="dark" className="navbar-dark navbar-expand-sm" fixed="top">
                    <NavbarBrand href="/" to="/" tag={Link}>Analytics</NavbarBrand>
                    <NavbarToggler/>
                    <Collapse navbar>
                        { isAuthenticated ? userLinks : guestLinks }
                    </Collapse>
                </Navbar>
            </div>
        );
    }
}

NavigationBar.propTypes = {
    auth: PropTypes.object.isRequired,
    logout: PropTypes.func.isRequired
}

function mapStateToProps(state) {
    return {
        auth: state.auth
    };
}

export default connect(mapStateToProps, { logout })(NavigationBar);
