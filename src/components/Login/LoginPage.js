import React from 'react';
import { Row, Col, Card, CardBody } from 'reactstrap';
import LoginForm from './LoginForm';
import {connect} from "react-redux";
import {login} from "../../actions/authActions";
import PropTypes from "prop-types";

class LoginPage extends React.Component{
    submit = data =>
        this.props.login(data).then(() => this.props.history.push("/home"));

    render(){
        return (
            <Row>
                <Col md={{ size: 4, offset: 4 }}>
                    <Card>
                        <CardBody>
                            <LoginForm submit={this.submit}  />
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        );
    }
}

LoginPage.propTypes = {
    login: PropTypes.func.isRequired
}

export default connect(null, { login })(LoginPage);