import React from 'react';
import { Form, FormGroup, Label, Input, Button, FormFeedback } from 'reactstrap';
import PropTypes from 'prop-types';
import validateInput from '../../validations/login';

class LoginForm extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            data:{
                username: "",
                password: "",
            },
            errors: {},
            isLoading: false
        }
    }

    isValid(){
        const { errors, isValid } = validateInput(this.state.data);

        if(!isValid){
            this.setState({ errors: errors });
        }

        return isValid;
    }

    onSubmit = (e)=>{
        e.preventDefault();
        if(this.isValid()){
            this.setState({ errors:{}, isLoading:true });
            this.props
                .submit(this.state.data)
                .catch(err =>
                    this.setState({ errors: err.response.data.errors, loading: false })
                );
        }
    }

    onChange = (e)=>{
        if (!!this.state.errors[e.target.name]) {
            let errors = Object.assign({}, this.state.errors);
            delete errors[e.target.name];
            this.setState({
                data: { ...this.state.data, [e.target.name]: e.target.value },
                errors
            });
        } else {
            this.setState({
                data: { ...this.state.data, [e.target.name]: e.target.value }
            });
        }
    }

    render(){
        const { errors, isLoading } = this.state;
        const { username, password } = this.state.data;

        return(
            <Form onSubmit={this.onSubmit}>
                <FormGroup>
                    <Label for="username">Username</Label>
                    <Input
                        type="text"
                        name="username"
                        id="username"
                        value={username}
                        onChange={this.onChange}
                        invalid={errors.username && errors.username.length>0}
                    />
                    {errors.username && <FormFeedback>{errors.username}</FormFeedback>}
                </FormGroup>

                <FormGroup>
                    <Label for="password">Password</Label>
                    <Input
                        type="password"
                        name="password"
                        id="password"
                        value={password}
                        onChange={this.onChange}
                        invalid={errors.password && errors.password.length>0}
                    />
                    {errors.password && <FormFeedback>{errors.password}</FormFeedback>}
                </FormGroup>

                <Button color="primary" disabled={isLoading}>Submit</Button>
            </Form>
        )
    }
}

LoginForm.propTypes = {
    submit: PropTypes.func.isRequired
};

export default LoginForm;