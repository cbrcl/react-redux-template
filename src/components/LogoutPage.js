import React from 'react';
import {connect} from "react-redux";
import {logout} from "../actions/authActions";


class LogoutPage extends React.Component{
    componentWillMount(){
        this.props.logout();
        this.props.history.push("/");
    }

    render(){
        return(
            <h1>Logout</h1>
        )
    }
}

export default connect(null, { logout })(LogoutPage);