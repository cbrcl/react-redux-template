import jwtDecode from 'jwt-decode';
import { SET_CURRENT_USER } from "../constants/actionTypes";
import api from '../utils/api';


export function setCurrentUser(user) {
    return {
        type: SET_CURRENT_USER,
        payload: user
    }
}

export function login(data){
    return dispatch => {
        return api.post('auth',data).then(data=>{
            const token = data.access_token;
            localStorage.setItem('jwtToken', token);
            dispatch(setCurrentUser(jwtDecode(token)));
        });
    }
}

export function logout() {
    return dispatch => {
        localStorage.removeItem('jwtToken');
        dispatch(setCurrentUser({}));
    }
}