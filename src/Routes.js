import React from "react";
import { Route, Switch } from "react-router-dom";
import AppliedRoute from "./components/AppliedRoute";

import Index from './components/Index';
import Home from './components/Home';
import NotFound from './components/NotFound';

import LoginPage from './components/Login/LoginPage';
import LogoutPage from './components/LogoutPage';

import requireAuth from './utils/requireAuth';

export default () =>
    <Switch>
        <AppliedRoute path="/" exact component={Index} />
        <AppliedRoute path="/home" exact component={requireAuth(Home)} />
        <AppliedRoute path="/login" exact component={LoginPage} />
        <AppliedRoute path="/logout" exact component={LogoutPage} />
        <Route component={NotFound} />
    </Switch>;