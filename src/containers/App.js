import React from 'react';
import Routes from "../Routes";
import NavigationBar from "../components/NavigationBar";

class App extends React.Component {
    render() {
        return (
            <div>
                <NavigationBar/>
                <Routes />
            </div>
        );
    }
}

export default App;
