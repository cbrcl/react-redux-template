## REACT
    create-react-app dev
    yarn add react-router-dom    
    npm install --save bootstrap reactstrap@next redux react-redux redux-thunk redux-logger prop-types
    
    npm install --save validator lodash
    
    npm install --save jwt-decode

    cd src
    mkdir actions components containers reducers constants    
    touch store.js routes.js containers/App.js reducers/index.js constants/actionTypes.js
    

## GIT
    cd existing_folder
    git init
    git remote add origin git@gitlab.com:cbrcl/strava_web.git
    git add .
    git commit -m "Initial commit"
    git push -u origin master

